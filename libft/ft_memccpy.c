/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 10:17:14 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:12:02 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *d, const void *s, int chr, size_t len)
{
	unsigned char		*dest;
	const unsigned char	*src;

	dest = (unsigned char *)d;
	src = (const unsigned char *)s;
	while (len-- > 0)
	{
		*dest++ = *src++;
		if (*(dest - 1) == (unsigned char)chr)
		{
			return (dest);
		}
	}
	return (NULL);
}
