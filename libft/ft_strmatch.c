/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmatch.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/14 10:34:12 by dansart           #+#    #+#             */
/*   Updated: 2015/12/30 19:12:26 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strmatch(char *str, char *search)
{
	if (*str == *search)
		return (*str ? ft_strmatch(str + 1, search + 1) : 1);
	if (*search == '*')
	{
		return (*str ? ft_strmatch(str + 1, search)
		+ ft_strmatch(str, search + 1) : ft_strmatch(str, search + 1));
	}
	return (0);
}
