/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:06:42 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:15:29 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *str1, char const *str2)
{
	char	*result;

	if ((!str1) ^ (!str2))
	{
		str1 = str1 ? str1 : "";
		str2 = str2 ? str2 : "";
	}
	if (!str1 && !str2)
		return (NULL);
	result = ft_strnew(ft_strlen(str1) + ft_strlen(str2));
	if (!result)
		return (NULL);
	return (ft_strcat(ft_strcat(result, str1), str2));
}
