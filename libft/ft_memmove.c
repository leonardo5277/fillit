/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 11:09:02 by dansart           #+#    #+#             */
/*   Updated: 2016/02/17 13:36:58 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	*ft_memcpyr(void *d, const void *s, size_t len)
{
	unsigned char		*dest;
	const unsigned char	*src;

	dest = (unsigned char *)d;
	src = (const unsigned char *)s;
	while (len-- > 0)
		dest[len] = src[len];
	return (d);
}

void		*ft_memmove(void *dest, const void *src, size_t len)
{
	if (src < dest && src + len > dest)
		return (ft_memcpyr(dest, src, len));
	return (ft_memcpy(dest, src, len));
}
