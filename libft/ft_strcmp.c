/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:24:30 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:14:36 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#define MAX(X, Y) ((X) < (Y) ? (X) : (Y))

int	ft_strcmp(const char *str1, const char *str2)
{
	return (ft_strncmp(str1, str2, MAX(ft_strlen(str1), ft_strlen(str2)) + 1));
}
