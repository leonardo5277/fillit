/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 11:49:42 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:15:37 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#define MIN(X, Y) ((X) <= (Y) ? (X) : (Y))
#define FT_STRNLEN(X, Y) MIN(Y, ft_strlen(X))

size_t	ft_strlcat(char *dest, const char *src, size_t len)
{
	size_t	dest_len;
	size_t	free_room;
	size_t	copy_len;

	dest_len = FT_STRNLEN(dest, len);
	free_room = len - dest_len;
	if (free_room == 0)
	{
		return (dest_len + ft_strlen(src));
	}
	copy_len = FT_STRNLEN(src, free_room - 1);
	ft_memcpy(dest + dest_len, src, copy_len);
	*(dest + dest_len + copy_len) = '\0';
	return (dest_len + ft_strlen(src));
}
