/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 12:57:04 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:12:08 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int chr, size_t len)
{
	const unsigned char	*str;

	str = (const unsigned char *)s;
	while (len-- > 0)
	{
		if (*str++ == (unsigned char)chr)
		{
			return ((void *)(str - 1));
		}
	}
	return (NULL);
}
