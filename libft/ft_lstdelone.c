/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 09:35:45 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:47:17 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list **list, void (*f)(void *, size_t))
{
	f((*list)->content, (*list)->content_size);
	free(*list);
	*list = NULL;
}
