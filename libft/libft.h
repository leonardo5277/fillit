/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/01 18:54:25 by dansart           #+#    #+#             */
/*   Updated: 2016/06/01 15:59:26 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

int					ft_tolower(int c);
int					ft_toupper(int c);
int					ft_atoi(const char *str);
int					ft_isalnum(int c);
int					ft_isalpha(int c);
int					ft_isascii(int c);
int					ft_isdigit(int c);
int					ft_isprint(int c);
char				*ft_itoa(int c);
void				ft_bzero(void *str, size_t size);
void				*ft_memalloc(size_t size);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memchr(const void *str, int c, size_t n);
int					ft_memcmp(const void *str1, const void *str2, size_t n);
void				*ft_memcpy(void *str1, const void *str2, size_t n);
void				ft_memdel(void **str);
void				*ft_memmove(void *str1, const void *str2, size_t n);
void				*ft_memset(void *str, int c, size_t n);
void				ft_putchar_fd(char c, int fd);
void				ft_putchar(char c);
void				ft_putendl_fd(char const *str, int fd);
void				ft_putendl(char const *str);
void				ft_putnbr_fd(int nb, int fd);
void				ft_putnbr(int fd);
void				ft_putstr_fd(char const *str, int fd);
void				ft_putstr(char const *str);
char				*ft_strcat(char *str1, const char *str2);
char				*ft_strchr(const char *str, int c);
void				ft_strclr(char *str);
int					ft_strcmp(const char *str1, const char *str2);
char				*ft_strcpy(char *dest, const char *src);
void				ft_strdel(char **str);
char				*ft_strdup(const char *str);
int					ft_strequ(char const *str1, char const *str2);
void				ft_striter(char *str, void (*f)(char *str));
void				ft_striteri(char *str, void (*f)(unsigned int, char *));
char				*ft_strjoin(char const *str1, char const *str2);
size_t				ft_strlcat(char *str, const char *c, size_t index);
size_t				ft_strlen(const char *str);
char				*ft_strmap(char const *str, char (*f)(char c));
char				*ft_strmapi(char const *str, char (*f)(unsigned int, char));
char				*ft_strncat(char *dest, const char *str, size_t len);
int					ft_strncmp(const char *str1, const char *str2, size_t len);
char				*ft_strncpy(char *str1, const char *str2, size_t len);
int					ft_strnequ(char const *str1, char const *str2, size_t len);
char				*ft_strnew(size_t size);
char				*ft_strnstr(const char *str1, const char *str2, size_t len);
char				*ft_strrchr(const char *str, int c);
char				**ft_strsplit(char const *str, char c);
char				*ft_strstr(const char *str1, const char *str2);
char				*ft_strsub(char const *str, unsigned int c, size_t size);
char				*ft_strtrim(char const *str);
void				ft_strarray_free(char **str);
size_t				ft_strarray_size(char **str);
size_t				ft_strcharamount(char *str, char c);
size_t				ft_strmatch(char *str1, char *str2);
t_list				*ft_lstnew(void const *content, size_t size);
t_list				*ft_lstmap(t_list *list, t_list *(*f)(t_list *));
void				ft_lstiter(t_list *list, void (*f)(t_list *));
void				ft_lstadd(t_list **list, t_list *newlist);
void				ft_lstdelone(t_list **list, void (*f)(void *, size_t));
void				ft_lstdel(t_list **list, void (*f)(void *, size_t));

#endif
