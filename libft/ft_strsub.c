/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:05:22 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:17:16 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *str, unsigned int start, size_t len)
{
	char	*result;

	if (!str)
		return (NULL);
	result = ft_strnew(len);
	if (!result)
		return (NULL);
	str += start;
	return (ft_strncpy(result, str, len));
}
