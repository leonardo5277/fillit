/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 18:13:46 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:16:16 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t len)
{
	size_t	pos;

	pos = ft_strlen(dest);
	if (ft_strlen(src) < len)
		len = ft_strlen(src);
	ft_strncpy(dest + pos, src, len);
	*(dest + pos + len) = '\0';
	return (dest);
}
