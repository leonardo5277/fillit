/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:38:27 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:15:07 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strequ(char const *str1, char const *str2)
{
	if ((str1 == NULL) || (str2 == NULL))
		return (0);
	if (str1 == str2)
		return (1);
	return (!ft_strcmp(str1, str2));
}
