/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:40:46 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:16:38 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strnequ(char const *str1, char const *str2, size_t len)
{
	if ((str1 == NULL) || (str2 == NULL))
		return (0);
	if (str1 == str2)
		return (1);
	return (!ft_strncmp(str1, str2, len));
}
