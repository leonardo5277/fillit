/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 10:05:36 by dansart           #+#    #+#             */
/*   Updated: 2016/02/17 13:36:04 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *d, const void *s, size_t len)
{
	unsigned char		*dest;
	const unsigned char	*src;

	dest = (unsigned char *)d;
	src = (const unsigned char *)s;
	while (len-- > 0)
		*dest++ = *src++;
	return (d);
}
