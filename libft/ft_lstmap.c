/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 09:22:50 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:56:15 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *list, t_list *(*f)(t_list *))
{
	t_list	*new_list;
	t_list	*item;

	new_list = NULL;
	while (list)
	{
		if (new_list == NULL)
		{
			new_list = f(list);
			item = new_list;
		}
		else
		{
			item->next = f(list);
			item = item->next;
		}
		list = list->next;
	}
	return (new_list);
}
