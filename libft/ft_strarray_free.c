/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarray_free.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/05 17:46:50 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:13:47 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_strarray_free(char **strarray)
{
	size_t	i;

	i = 0;
	while (strarray[i])
	{
		free(strarray[i]);
		++i;
	}
	free(strarray);
}
