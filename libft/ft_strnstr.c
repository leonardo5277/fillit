/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:20:30 by dansart           #+#    #+#             */
/*   Updated: 2016/02/17 14:04:35 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *source, const char *search, size_t size)
{
	size_t	len;

	if (!*search)
		return ((char *)source);
	len = ft_strlen(search);
	while ((*source) && (size-- >= len))
	{
		if ((*source == *search) && (ft_memcmp(source, search, len) == 0))
			return ((char *)source);
		source++;
	}
	return (NULL);
}
