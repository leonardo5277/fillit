/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 09:37:54 by dansart           #+#    #+#             */
/*   Updated: 2016/02/17 17:28:42 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **list, void (*f)(void *, size_t))
{
	t_list	*item;

	while (*list)
	{
		item = (*list)->next;
		f((*list)->content, (*list)->content_size);
		free(*list);
		*list = item;
	}
}
