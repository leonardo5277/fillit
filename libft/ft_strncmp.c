/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:24:30 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:16:24 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *str1, const char *str2, size_t len)
{
	unsigned char	c1;
	unsigned char	c2;

	if (str1 == str2)
		return (0);
	while (len-- > 0)
	{
		c1 = (unsigned char)*str1;
		c2 = (unsigned char)*str2;
		if (c1 != c2)
		{
			return (c1 - c2);
		}
		if (c1 == '\0')
		{
			return (0);
		}
		str1++;
		str2++;
	}
	return (0);
}
