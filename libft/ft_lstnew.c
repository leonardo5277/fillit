/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 09:19:20 by dansart           #+#    #+#             */
/*   Updated: 2016/02/17 17:30:39 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t size)
{
	t_list	*new;

	new = ft_memalloc(sizeof(t_list));
	if (new == NULL)
		return (NULL);
	if (size > 0)
	{
		new->content = ft_memalloc(size);
		if (new->content == NULL)
		{
			free(new);
			return (NULL);
		}
		ft_memcpy(new->content, content, size);
	}
	new->content_size = size;
	return (new);
}
