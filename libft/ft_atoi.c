/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:32:09 by dansart           #+#    #+#             */
/*   Updated: 2015/12/18 09:10:57 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_isspace(char c)
{
	int	res;

	res = (c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\r' ||
			c == '\f');
	return (res);
}

int			ft_atoi(const char *str)
{
	int	res;
	int	mul;

	res = 0;
	mul = 1;
	while (ft_isspace(*str))
	{
		str++;
	}
	if (*str == '-' || *str == '+')
	{
		mul = (*str == '-' ? -1 : 1);
		str++;
	}
	while (ft_isdigit(*str))
	{
		res = res * 10 + (*str - '0');
		str++;
	}
	return (mul * res);
}
