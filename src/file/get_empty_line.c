/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_empty_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 15:04:59 by dansart           #+#    #+#             */
/*   Updated: 2016/06/01 15:48:20 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "file.h"
#include "libft.h"
#include "error.h"

int	get_empty_line(int fd)
{
	char	c;
	int		res;

	res = read(fd, &c, 1);
	if (res == -1)
		error();
	if (res == 0)
		return (0);
	if (c == '\n')
		return (1);
	error();
	return (0);
}
