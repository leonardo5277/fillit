/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 12:05:50 by dansart           #+#    #+#             */
/*   Updated: 2016/01/04 13:16:21 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include "board.h"
#include "error.h"
#include "file.h"

t_board	parse_file(char *filename)
{
	int		fd;
	t_piece	*list;
	t_board	board;
	char	letter;

	if ((fd = open(filename, O_RDONLY)) == -1)
		error();
	letter = 'A';
	list = get_piece(fd, letter);
	while (get_empty_line(fd))
		add_piece(list, get_piece(fd, ++letter));
	if (pieces_qty(list) > 26)
		error();
	close(fd);
	board = new_board(list);
	return (board);
}
