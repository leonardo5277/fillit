/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 23:08:30 by dansart           #+#    #+#             */
/*   Updated: 2016/06/05 16:05:08 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "error.h"
#include "board.h"
#include "file.h"

int	main(int argc, char *argv[])
{
	t_board	board;

	if (argc != 2)
		return (usage(argv[0]));
	board = parse_file(argv[1]);
	board = solve_board(board);
	display_result(board);
	return (0);
}
