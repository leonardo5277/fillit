/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wrap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 23:42:53 by dansart           #+#    #+#             */
/*   Updated: 2016/01/03 11:46:05 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "point.h"

void	wrap(t_point *point)
{
	point->x = 0;
	point->y = point->y + 1;
}
