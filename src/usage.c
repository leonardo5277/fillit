/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 15:41:54 by dansart           #+#    #+#             */
/*   Updated: 2016/05/25 17:04:24 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "error.h"

int	usage(char *name)
{
	ft_putstr("usage: ");
	ft_putstr(name);
	ft_putendl(" source_file");
	return (0);
}
