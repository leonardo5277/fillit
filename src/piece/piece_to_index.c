/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece_to_index.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 22:25:48 by dansart           #+#    #+#             */
/*   Updated: 2016/01/03 22:27:27 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece.h"

int	piece_to_index(t_piece *piece, t_point coord)
{
	return (coord.y * piece->width + coord.x);
}
