/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_piece.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 12:15:25 by dansart           #+#    #+#             */
/*   Updated: 2016/01/03 12:16:32 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece.h"

void	add_piece(t_piece *list, t_piece *new)
{
	while (list->next)
		list = list->next;
	list->next = new;
}
