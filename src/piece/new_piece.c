/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_piece.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 12:10:03 by dansart           #+#    #+#             */
/*   Updated: 2016/05/24 18:13:08 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "piece.h"
#include "error.h"

t_piece	*new_piece(char letter)
{
	t_piece	*piece;

	piece = ft_memalloc(sizeof(t_piece));
	if (!piece)
		error();
	piece->width = 4;
	piece->shape = ft_strnew(16);
	piece->letter = letter;
	return (piece);
}
