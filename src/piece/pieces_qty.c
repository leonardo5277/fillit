/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pieces_qty.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 12:18:37 by dansart           #+#    #+#             */
/*   Updated: 2016/01/03 12:18:39 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece.h"

int	pieces_qty(t_piece *list)
{
	int	total;

	total = 0;
	while (list)
	{
		list = list->next;
		++total;
	}
	return (total);
}
