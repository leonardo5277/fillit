/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece_to_coord.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 22:25:18 by dansart           #+#    #+#             */
/*   Updated: 2016/01/03 22:25:19 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece.h"

t_point	piece_to_coord(t_piece *piece, int index)
{
	t_point point;

	point.y = index / piece->width;
	point.x = index % piece->width;
	return (point);
}
