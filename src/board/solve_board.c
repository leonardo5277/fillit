/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_board.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 10:58:32 by dansart           #+#    #+#             */
/*   Updated: 2016/05/24 18:01:34 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "board.h"
#include "error.h"

t_board	solve_board(t_board board)
{
	while (!solve_piece(board, board.pieces))
	{
		++(board.size);
		free(board.blocks);
		board.blocks = ft_strnew(board.size * board.size);
		if (!board.blocks)
			error();
		ft_memset(board.blocks, '.', board.size * board.size);
	}
	return (board);
}
