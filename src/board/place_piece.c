/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place_piece.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 13:48:35 by dansart           #+#    #+#             */
/*   Updated: 2016/01/04 14:24:58 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"

void	place_piece(t_board board, t_piece *piece, t_point anchor)
{
	int	i;
	int	j;

	i = 0;
	while (i < piece->width * piece->height)
	{
		j = board_to_index(board, ADD_POINTS(piece_to_coord(piece, i), anchor));
		if (piece->shape[i] == '#')
			board.blocks[j] = piece->letter;
		++i;
	}
}
