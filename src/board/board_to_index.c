/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   board_to_index.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 22:28:48 by dansart           #+#    #+#             */
/*   Updated: 2016/01/03 22:28:57 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"

int	board_to_index(t_board board, t_point coord)
{
	return (coord.y * board.size + coord.x);
}
