/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_piece.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 13:18:19 by dansart           #+#    #+#             */
/*   Updated: 2016/05/24 18:18:31 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "board.h"

int	solve_piece(t_board board, t_piece *piece)
{
	t_point	anchor;

	if (piece == NULL)
		return (1);
	anchor = point(0, 0);
	while (anchor.y <= board.size - piece->height)
	{
		anchor.x = 0;
		while (anchor.x <= board.size - piece->width)
		{
			if (piece_fits(board, piece, anchor))
			{
				place_piece(board, piece, anchor);
				if (solve_piece(board, piece->next))
					return (1);
				remove_piece(board, piece, anchor);
			}
			++(anchor.x);
		}
		++(anchor.y);
	}
	return (0);
}
