/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece_fits.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 13:40:52 by dansart           #+#    #+#             */
/*   Updated: 2016/01/04 14:43:39 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"

int	piece_fits(t_board board, t_piece *piece, t_point anchor)
{
	int	i;
	int	j;

	if (piece->width > board.size || piece->height > board.size)
		return (0);
	i = 0;
	while (i < piece->width * piece->height)
	{
		j = board_to_index(board, ADD_POINTS(piece_to_coord(piece, i), anchor));
		if (piece->shape[i] == '#' && board.blocks[j] != '.')
			return (0);
		++i;
	}
	return (1);
}
