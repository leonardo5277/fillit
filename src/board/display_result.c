/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_result.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 13:57:52 by dansart           #+#    #+#             */
/*   Updated: 2016/01/04 14:05:10 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "board.h"

void	display_result(t_board board)
{
	int i;

	i = 0;
	while (i < board.size)
	{
		write(1, board.blocks + board.size * i, board.size);
		write(1, "\n", 1);
		++i;
	}
}
