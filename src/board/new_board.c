/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_board.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 23:47:22 by dansart           #+#    #+#             */
/*   Updated: 2016/05/24 18:13:21 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "board.h"
#include "error.h"

t_board	new_board(t_piece *pieces)
{
	t_board	board;
	int		size;
	int		qty;

	qty = pieces_qty(pieces) * 4;
	size = 1;
	while (size * size < qty)
		++size;
	board.size = size;
	board.blocks = ft_strnew(board.size * board.size);
	if (!board.blocks)
		error();
	ft_memset(board.blocks, '.', board.size * board.size);
	board.pieces = pieces;
	return (board);
}
