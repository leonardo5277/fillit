/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 23:27:47 by dansart           #+#    #+#             */
/*   Updated: 2016/05/24 18:17:36 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POINT_H
# define POINT_H
# define ADD_POINTS(a, b) (point((a).x + (b).x, (a).y + (b).y))

typedef struct	s_point
{
	int			x;
	int			y;
}				t_point;

t_point			point(int x, int y);
void			right(t_point *point);
void			wrap(t_point *point);

#endif
