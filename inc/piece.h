/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 23:34:26 by dansart           #+#    #+#             */
/*   Updated: 2016/05/24 18:18:00 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIECE_H
# define PIECE_H
# include "point.h"

typedef struct			s_piece
{
	int					width;
	int					height;
	char				*shape;
	char				letter;
	struct s_piece		*next;
}						t_piece;

t_piece					*new_piece(char letter);
void					trim_piece(t_piece *piece);
int						pieces_qty(t_piece *piece);
void					add_piece(t_piece *list, t_piece *new);
t_point					piece_to_coord(t_piece *piece, int index);
int						piece_to_index(t_piece *piece, t_point point);

#endif
