/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 12:38:53 by dansart           #+#    #+#             */
/*   Updated: 2016/01/04 13:54:00 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILE_H
# define FILE_H
# include "piece.h"
# include "board.h"

t_piece	*get_piece(int fd, int letter);
int		get_empty_line(int fd);
t_board	parse_file(char *filename);

#endif
