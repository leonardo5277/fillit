/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   board.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dansart <dansart@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/02 23:24:58 by dansart           #+#    #+#             */
/*   Updated: 2016/01/04 14:49:30 by dansart          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOARD_H
# define BOARD_H
# include "piece.h"
# include "point.h"

typedef struct	s_board
{
	int			size;
	char		*blocks;
	t_piece		*pieces;
}				t_board;

t_board			new_board(t_piece *pieces);
void			place_piece(t_board board, t_piece *piece, t_point point);
void			remove_piece(t_board board, t_piece *piece, t_point point);
int				piece_fits(t_board board, t_piece *piece, t_point point);
int				board_to_index(t_board board, t_point coord);
t_board			solve_board(t_board board);
int				solve_piece(t_board board, t_piece *piece);
void			display_result(t_board board);

#endif
