# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dansart <dansart@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/31 11:27:31 by dansart           #+#    #+#              #
#    Updated: 2016/09/29 17:39:58 by lmunoz           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

CFLAGS = -Wall -Wextra -Werror

CLIB = -Llibft -lft

INC = inc \
	  libft

INCDIRS = $(addprefix -I,$(INC))

NAME = fillit

SRCFILES = main.c \
		   error.c \
		   point.c \
		   right.c \
		   wrap.c \
		   new_board.c \
		   piece_to_index.c \
		   piece_to_coord.c \
		   board_to_index.c \
		   add_piece.c \
		   new_piece.c \
		   pieces_qty.c \
		   get_piece.c \
		   parse_file.c \
		   get_empty_line.c \
		   trim_piece.c \
		   solve_board.c \
		   solve_piece.c \
		   piece_fits.c \
		   place_piece.c \
		   remove_piece.c \
		   display_result.c \
		   usage.c

DIRS = point \
	   board \
	   piece \
	   file


SRCDIRS := $(addprefix src/,$(DIRS))

vpath %.c src $(SRCDIRS)

OBJDIR = obj

OBJECTS = $(SRCFILES:%.c=$(OBJDIR)/%.o)

all: $(NAME)

$(NAME): libft/libft.a $(OBJECTS)
	$(CC) $(INCDIRS)  $(CLIB) -o $@ $(OBJECTS)

$(OBJDIR)/%.o : %.c | $(OBJDIR)
	$(CC) $(INCDIRS) $(CFLAGS) -c $< -o $@

$(OBJDIR):
	@mkdir -p $@

libft/libft.a: force
	@$(MAKE) -w -C libft/

clean:
	@rm -rf $(OBJDIR)
	@$(MAKE) -w -C libft/ clean

fclean: clean
	@rm -f $(NAME)
	@$(MAKE) -w -C libft/ fclean

re: fclean all

force:
	@true

.PHONY: all clean fclean re

